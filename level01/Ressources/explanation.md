As we found on the previous step there is a password for `flag01` user inside `/etc/passwd`.  
Copying password file on host machine `scp -P 4242 level00@192.168.56.101:/etc/passwd . `
Decrypt it using `John` tool.

Command: `john passwd`  
Result: `flag01:abcdefg:3001:3001::/home/flag/flag01:/bin/bash`

User: `flag01`  
Password: `abcdefg`  
Flag: `f2av5il02puano7naaf6adaaf`