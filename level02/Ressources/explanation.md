Found file `level02.pcap` in home directory. As it was discovered this file is related to network communication.

Open it using free opensource tool `Wireshark`
> More info on `https://www.wireshark.org`  

![alt text](./Screenshot.png)


User: `flag02`  
Password: `ft_wandrNDRelL0L`  
Flag: `kooda2puivaav1idi4f57q8iq`