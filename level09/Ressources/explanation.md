Inside the home folder, we can find two files owned by `flag09` user.

Command: `ls -l`  
Output:
```
total 12
-rwsr-sr-x 1 flag09 level09 7640 Mar  5  2016 level09
----r--r-- 1 flag09 level09   26 Mar  5  2016 token
```

`token` file has some data inside and it looks like it encrypted.
```
f4kmm6p|=�p�n��DB�Du{��
```


`level09` - binary file, and it protects from reverse engineering. Discovered internal content with `radar2` tool shows that there is a lot of protection logic and only one loop which goes through the first argument and shift each element and print it to stdout.

Command: `./level09 aaaaaaaaaaaaaaaaaaaaaaaaaa`  
Output: `abcdefghijklmnopqrstuvwxyz`

So I created a simple `C` program which just read `token` content and shifts it back and print result.
```
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
    int fd = -1;

    if (argc == 2
        && (fd = open(argv[1], O_RDONLY)) > 2) 
    {
        unsigned char buff[26];
        read(fd, buff, 26);
        for (int i = 0; i < 26; i++)
            buff[i] = buff[i] - i;

        printf("%s\n", (char*)buff);
        close(fd);
    }
    else 
        printf("Error\n");
}
```
Command: `./a.out ~/token`  
Output: `f3iji1ju5yuevaus41q1afiuq�`

User: `flag09`  
Password: `f3iji1ju5yuevaus41q1afiuq`

Flag: `s5cAJpM8ev6XHw998pRWG728z`