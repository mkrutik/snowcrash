There is one binary file in home folder.  
Command: `ls -l`  
Output:
```
total 8
-rwsr-sr-x 1 flag13 level13 7303 Aug 30  2015 level13
```

If we run it we will see such output : `UID 2013 started us but we we expect 4242`.  
Inside this binary just check `getuid` syscall result and compare it with `4242`.

I used `strace` functionality to Inject syscall result value (`strace` used on host machine).  
Command: `strace -e inject=getuid32:retval=4242 ./level13`  
Output:
```
execve("./level13", ["./level13"], 0x7ffc59dfe170 /* 66 vars */) = 0
strace: [ Process PID=18160 runs in 32 bit mode. ]
brk(NULL)                               = 0x963e000
arch_prctl(0x3001 /* ARCH_??? */, 0xffc1bba8) = -1 EINVAL (Invalid argument)
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
mmap2(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0xf7f79000
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_LARGEFILE|O_CLOEXEC) = 3
fstat64(3, {st_mode=S_IFREG|0644, st_size=87712, ...}) = 0
mmap2(NULL, 87712, PROT_READ, MAP_PRIVATE, 3, 0) = 0xf7f63000
close(3)                                = 0
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/lib32/libc.so.6", O_RDONLY|O_LARGEFILE|O_CLOEXEC) = 3
read(3, "\177ELF\1\1\1\3\0\0\0\0\0\0\0\0\3\0\3\0\1\0\0\0\220\360\1\0004\0\0\0"..., 512) = 512
pread64(3, "\4\0\0\0\24\0\0\0\3\0\0\0GNU\0\r\371y\270\262D)K\274)\273\350\367\366\335k"..., 96, 468) = 96
fstat64(3, {st_mode=S_IFREG|0755, st_size=2002268, ...}) = 0
pread64(3, "\4\0\0\0\24\0\0\0\3\0\0\0GNU\0\r\371y\270\262D)K\274)\273\350\367\366\335k"..., 96, 468) = 96
mmap2(NULL, 2010892, PROT_READ, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0xf7d78000
mmap2(0xf7d95000, 1409024, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1d000) = 0xf7d95000
mmap2(0xf7eed000, 458752, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x175000) = 0xf7eed000
mmap2(0xf7f5d000, 16384, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1e4000) = 0xf7f5d000
mmap2(0xf7f61000, 7948, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0xf7f61000
close(3)                                = 0
set_thread_area({entry_number=-1, base_addr=0xf7f7a0c0, limit=0x0fffff, seg_32bit=1, contents=0, read_exec_only=0, limit_in_pages=1, seg_not_present=0, useable=1}) = 0 (entry_number=12)
mprotect(0xf7f5d000, 8192, PROT_READ)   = 0
mprotect(0x8049000, 4096, PROT_READ)    = 0
mprotect(0xf7faa000, 4096, PROT_READ)   = 0
munmap(0xf7f63000, 87712)               = 0
getuid32()                              = 4242 (INJECTED)
brk(NULL)                               = 0x963e000
brk(0x965f000)                          = 0x965f000
brk(0x9660000)                          = 0x9660000
fstat64(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x2), ...}) = 0
write(1, "your token is 2A31L79asukciNyi8u"..., 40your token is 2A31L79asukciNyi8uppkEuSx
) = 40
exit_group(40)                          = ?
+++ exited with 40 +++
```

Flag: `2A31L79asukciNyi8uppkEuSx`