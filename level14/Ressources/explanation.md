For this challenge, I didn't find any special files owned by `level14` or `flag14` user.
So I just reverse engineering `getflag` ))

There are few checks inside which checks that binary is not running under debugger tools. At the end - switch case on `getuid` value and decryption on the predefined string.

As in previous chalange I used `strace`.  
Command: `strace -o out -e inject=ptrace:retval=0 -e inject=getuid32:retval=3014 ./getflag`  
Output: `Check flag.Here is your token : 7QiHafiNa3HVozsaXkawuYrTstxbpABHD8CPnHJ`

User: `flag14`  
Passord: `7QiHafiNa3HVozsaXkawuYrTstxbpABHD8CPnHJ`

Flag: `7QiHafiNa3HVozsaXkawuYrTstxbpABHD8CPnHJ`